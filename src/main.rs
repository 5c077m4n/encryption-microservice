extern crate actix_web;
extern crate dotenv;
extern crate env_logger;
extern crate openssl;
#[macro_use]
extern crate serde_derive;

mod routes;

use actix_web::{http::Method, middleware::Logger, server, App, HttpResponse};
use dotenv::dotenv;

const PORT: isize = 8080;

fn create_server_app() -> App {
	App::new()
		.middleware(Logger::default())
		.resource("/", |r| r.f(|_| HttpResponse::Ok()))
		.resource("/hashing", |r| r.f(|_| HttpResponse::Ok()))
		.resource("/hashing/hash", |r| {
			r.method(Method::POST).with(routes::hashing::hash_password)
		})
		.resource("/hashing/verify", |r| {
			r.method(Method::POST)
				.with(routes::hashing::verify_password)
		})
		.resource("/rsa-keys", |r| r.f(|_| HttpResponse::Ok()))
		.resource("/rsa-keys/create", |r| {
			r.method(Method::GET).f(routes::rsa_keys::create_keys)
		})
		.resource("/rsa-keys/public", |r| {
			r.method(Method::GET).f(routes::rsa_keys::get_public_key)
		})
}

fn main() {
	dotenv().ok();
	std::env::set_var("RUST_LOG", "actix_web=info");
	env_logger::init();

	server::new(create_server_app)
		.bind(format!("127.0.0.1:{}", PORT))
		.unwrap()
		.run();
}
