#[derive(Debug, Serialize, Deserialize)]
pub struct Message {
	pub message: String,
	pub status: u16,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Error {
	pub status: u16,
	pub error: String,
	pub message: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct HashData {
	pub password: String,
	pub hashed_content: String,
}
