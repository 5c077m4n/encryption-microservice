mod json_responses;

pub mod rsa_keys {
	use super::json_responses::*;
	use actix_web::{HttpRequest, HttpResponse};
	use openssl::rsa::Rsa;
	use std::{fs::create_dir_all, fs::File, io::prelude::*, path::Path};

	pub fn create_keys(_: &HttpRequest) -> HttpResponse {
		let rsa = Rsa::generate(4096).unwrap();
		let private_key = rsa
			.private_key_to_der()
			.expect("Failed to create the private key");
		let public_key = rsa
			.public_key_to_der()
			.expect("Failed to create the public key");

		create_dir_all(Path::new("./keys")).expect("There was an error in creating the folder");
		let mut private_key_file = File::create(Path::new("./keys/key").as_os_str())
			.expect("Failed to create a private key file.");
		let mut public_key_file = File::create(Path::new("./keys/key.pub").as_os_str())
			.expect("Failed to create a private key file.");

		private_key_file
			.write_all(&private_key)
			.expect("Failed to write the private key to a file.");
		public_key_file
			.write_all(&public_key)
			.expect("Failed to write the public key to a file.");

		HttpResponse::Created().json(Message {
			status: 201,
			message: "The two keys where created successfully.".to_string(),
		})
	}

	pub fn get_public_key(_: &HttpRequest) -> HttpResponse {
		let mut file = File::open("../keys/key.pub").expect("File not found.");
		let mut contents = String::new();

		file.read_to_string(&mut contents)
			.expect("Something went wrong reading the file.");

		HttpResponse::Ok().json(Message {
			status: 200,
			message: contents.to_string(),
		})
	}
}

pub mod hashing {
	use super::json_responses::*;
	use actix_web::{HttpResponse, Json};

	pub fn hash_password(req: Json<HashData>) -> HttpResponse {
		match bcrypt::hash(&req.password, bcrypt::DEFAULT_COST) {
			Ok(hash) => HttpResponse::Ok().json(Message {
				status: 200,
				message: hash.to_string(),
			}),
			Err(_) => HttpResponse::InternalServerError().json(Message {
				status: 500,
				message: "[bcrypt] There was an error in verifying the password and hash."
					.to_string(),
			}),
		}
	}

	pub fn verify_password(req: Json<HashData>) -> HttpResponse {
		match bcrypt::verify(&req.password, &req.hashed_content) {
			Ok(validity) => HttpResponse::Ok().json(Message {
				status: 200,
				message: validity.to_string(),
			}),
			Err(_) => HttpResponse::InternalServerError().json(Message {
				status: 500,
				message: "[bcrypt] There was an error in verifying the password and hash."
					.to_string(),
			}),
		}
	}
}
